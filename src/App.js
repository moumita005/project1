import './App.css';
import DropArea from './Component/Block';

function App() {
  return (
    <div className="App">
      <DropArea />
    </div>
  );
}

export default App;
