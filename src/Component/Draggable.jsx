import React, { Component } from 'react'

export default class Draggable extends Component {
  constructor(props) {
    super(props);
    this.ref = React.createRef();
  }
  onMouseDown =(e) =>{
    this.props.updateStateDragging( this.props.id, true );
  }
  onMouseUp=(e) =>{
    this.props.updateStateDragging( this.props.id, false );
  }
  onDragStart=(e) =>{
    const nodeStyle = this.ref.node?.style;
    e.dataTransfer.setData( 'application/json', JSON.stringify({
      id: this.props.id,
      x: e.clientX - parseInt(nodeStyle?.left),
      y: e.clientY - parseInt(nodeStyle?.top),
    }));
  }
  onDragEnd=(e) =>{
    this.props.updateStateDragging( this.props.id, false );
  }
  render() {
    let styles = {
      top: this.props.top,
      left: this.props.left,
    };
    return (
      <div
        ref={"node"}
        draggable={this.props.isDragging}
        id={'item_' + this.props.id}
        className="item"
        style={styles}
        onMouseDown={this.onMouseDown}
        onMouseUp={this.onMouseUp}
        onDragStart={this.onDragStart}
        onDragEnd={this.onDragEnd}>
        {'item_' + this.props.id}
      </div>
    )
  }
}
