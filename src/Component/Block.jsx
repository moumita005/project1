import React from 'react'
import { Component } from 'react';
import Draggable from './Draggable';

export default class DropArea extends Component {
  state = {
    list: [
      { id: 1, isDragging: false, top: 0, left: 0 },
      { id: 2, isDragging: false, top: 40, left: 150 },
    ],
  }
  onDragOver=(e) =>{
    e.preventDefault();
    return false;
  }
  onDrop=(e) =>{
    var obj = JSON.parse(e.dataTransfer.getData('application/json'));
    let list = this.state.list;
    let index = this.state.list.findIndex((item) => item.id == obj.id);
    list[index].isDragging = false;
    list[index].top = (e.clientY - obj.y);
    list[index].left = (e.clientX - obj.x);

    let newState = Object.assign(
      this.state, {
      list: list
    });
    this.setState(newState);

    e.preventDefault();
  }

  updateStateDragging = (id, isDragging) => {
    let list = this.state.list;
    let index = this.state.list.findIndex((item) => item.id == id);
    list[index].isDragging = isDragging;

    let newState = Object.assign(
      this.state, {
      list: list
    });
    this.setState(newState);
  }
  render() {
    let items = [];
    for (let item of this.state.list) {
      items.push(
        <Draggable
          key={item.id}
          id={item.id}
          top={item.top}
          left={item.left}
          isDragging={item.isDragging}
          updateStateDragging={this.updateStateDragging}
        />
      );
    }
    return (
      <div
        className="drop-area"
        onDragOver={this.onDragOver}
        onDrop={this.onDrop} >
        {items}
      </div>
    )
  }
}





